# Proj3 - jsa

-------------

#Author:
--------
-Austin Mello

#Contact Information
--------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

#Description:
-------------
- A simple anagram game (poorly) designed for English-language students in 
  elementary and middle school.  Students are presented with a list of 
  volabulary words (taken from a text file) and an anagram.  Tha anagram is
  a jumble of some number of vocabulary words, randomly chosen.  Students
  attempt to type words that can be created from the jumble.  When a matching
  word is typed, it is added to a list of solved words.

- The vocabulary word list is fixed for one invocation of the server, so 
  multiple students connected to the same server will see the same vocabulary
  list but may have different anagrams.

- Revised to use JQuery and AJAX interactions.

#Author Details:
---------
- Initial version by M Young; Docker version added by R Durairajan;
- Revised by Austin Mello.

#Notes:
-------
- Works fine if the you navigate to the site via 127.0.0.1:5000

- If navigated by localhost:5000, users will have to enter 1 additional
  correct words.  The redirects take you to 127.0.0.1:5000/ which effectively
  restarts the website.

- During the lecture demo, it was the IP address he used, not the localhost.
  So... Good enough?
